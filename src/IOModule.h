#ifndef IOModule_h
#define IOModule_h

#include <arduino.h>
#include <Menu.h>
#include <LiquidCrystal_I2C.h>
#include <CommandHandler.h>
#include <ShiftHandler.h>

#include <EEPROMex.h>

#define MODULE_NAME "IOModule"

class IOModule : public ICommandListener, public IOptionListener {

public:
    IOModule();

    void setup();
    void run();
    void powerOutage(long duration);
    void processCommand(Command* cmd);
    void timerCallback(Timer* timer);
    void optionCallback(Option* option);

    void refreshBatteries();
    void buildMenu();

  private:
    ShiftHandler* _shiftHandler;
    CommandHandler* _commandHandler;

    bool _refreshSerial = false;
    bool _menuEnabled = true;
    DisplayWriter_i2c* _displayWriter;
    Menu* _menu = NULL;
    /* General */
    BoolOption* _soundEnabledOption = NULL;
    ActionOption* _startBombOption = NULL;

    /* Properties */
    IntOption* _batteriesOption = NULL;
    BoolOption* _frkOption = NULL;
    BoolOption* _clrOption = NULL;
    BoolOption* _carOption = NULL;
    BoolOption* _serialPortOption = NULL;
    BoolOption* _parallelPortOption = NULL;

    /* Difficulty */
    BoolOption* _outagesEnabledOption = NULL;
    BoolOption* _alarmEnabledOption = NULL;
    IntOption* _maxStrikesOption = NULL;
    IntOption* _durationMinutesOption = NULL;
    IntOption* _durationSecondsOption = NULL;

    int _batteries = 1;
    char _serialNo[7];
    bool _frk = true;
    bool _car = true;
    bool _clr = true;
    bool _parallelPort = false;
    bool _serialPort = false;
};

#endif
