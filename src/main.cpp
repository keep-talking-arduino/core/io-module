#include <IOModule.h>

IOModule* ioModule;

void setup() {
  Serial.begin(9600);
  randomSeed(analogRead(A0) * analogRead(A1));

  ioModule = new IOModule();
  ioModule->setup();
}

void loop() {
  ioModule->run();
}