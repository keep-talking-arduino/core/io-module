#include <IOModule.h>

IOModule::IOModule(){

}

void IOModule::processCommand(Command* cmd){
  Serial.println(cmd->toString());

  int action = cmd->getCmdCode();

  if(action == CMD_OPERATION_START){
    _menuEnabled = false;
    _refreshSerial = true;
  }
  else if(action == CMD_OPERATION_STOP){
    _menuEnabled = true;
    _menu->refresh();
  }
  else if(action == CMD_SET_SERIALNO){
    cmd->getValue().toCharArray(_serialNo, 7);
    _refreshSerial = true;
  }
  else if(action == CMD_SET_BATTERIES){
    _batteries = cmd->getValue().toInt();
  }
  else if(action == CMD_SET_CAR){
    _car = cmd->getValue().toInt();
  }
  else if(action == CMD_SET_CLR){
    _clr = cmd->getValue().toInt();
  }
  else if(action == CMD_SET_FRK){
    _frk = cmd->getValue().toInt();
  }
  else if(action == CMD_SET_SERIALPORT){
    _parallelPort = cmd->getValue().toInt();
  }
  else if(action == CMD_SET_PARALLELPORT){
    _parallelPort = cmd->getValue().toInt();
  }
  else if(action == CMD_OPERATION_POWEROUTAGE){
    powerOutage(cmd->getValue().toInt());
  }
}

void IOModule::powerOutage(long duration){
  
}

void IOModule::setup(){

  Serial.println(MODULE_NAME);
  /* initialize & configure components */
  _displayWriter = new DisplayWriter_i2c(0x3F);

  buildMenu();

  _shiftHandler = ShiftHandler::getInstance();
  _shiftHandler->configure(11, 12, 10);

  _commandHandler = CommandHandler::getInstance();
  _commandHandler->configure(MODULE_IO, this);

  // /* Triggers properties to be visually up-to-date. */
  optionCallback(_durationMinutesOption);
  optionCallback(_batteriesOption);
  optionCallback(_frkOption);
  optionCallback(_carOption);
  optionCallback(_clrOption);
  
  Serial.println("Setup complete.");
}

void IOModule::run(){
  _commandHandler->run();
  _shiftHandler->update();

  if(_refreshSerial){
    _displayWriter->clear();
    _displayWriter->write("Serial #", 4, 0);
    _displayWriter->write(_serialNo, 5, 1);
    _refreshSerial = false;
  }
  
  if(_menuEnabled){
    _menu->run();
  }
}

void IOModule::optionCallback(Option* option){
  if(option == _startBombOption){
    _commandHandler->send(new Command(MODULE_CTRLUNIT, CMD_SET_BATTERIES, String(_batteries)));
    _commandHandler->send(new Command(MODULE_CTRLUNIT, CMD_SET_FRK, String(_frk)));
    _commandHandler->send(new Command(MODULE_CTRLUNIT, CMD_SET_CLR, String(_clr)));
    _commandHandler->send(new Command(MODULE_CTRLUNIT, CMD_SET_CAR, String(_car)));
    _commandHandler->send(new Command(MODULE_CTRLUNIT, CMD_SET_SERIALPORT, String(_serialPort)));
    _commandHandler->send(new Command(MODULE_CTRLUNIT, CMD_SET_PARALLELPORT, String(_parallelPort)));
    _commandHandler->send(new Command(MODULE_CTRLUNIT, CMD_OPERATION_START, ""));
  }
  else if(option == _batteriesOption){
    _batteries = _batteriesOption->getValue();
    refreshBatteries();
  }
  else if(option == _frkOption){
    _frk = _frkOption->getValue();
    _shiftHandler->setValue(0, 5, _frk);
  }
  // else if(option == _clrOption){
  //   _clr = _clrOption->getValue();
  //   _shiftHandler->setValue(0, 6, _clr);
  // }
  // else if(option == _carOption){
  //   _car = _carOption->getValue();
  //   _shiftHandler->setValue(0, 7, _car);
  // }
  // else if(option == _serialPortOption){
  //   _serialPort = _serialPortOption->getValue();
  //   _shiftHandler->setValue(1, 1, _serialPort);
  // }
  // else if(option == _parallelPortOption){
  //   _parallelPort = _parallelPortOption->getValue();
  //   _shiftHandler->setValue(1, 2, _parallelPort);
  // }
  else if(option == _durationMinutesOption || option == _durationSecondsOption){
    int durationMinutes = _durationMinutesOption->getValue();
    Serial.println(durationMinutes);
    int durationSeconds = _durationSecondsOption->getValue();
    Serial.println(durationSeconds);
    String timeString = String(durationMinutes) + String(",") + String(durationSeconds);
    Serial.println(timeString);
    Command* cmd = new Command(MODULE_CLOCK, CMD_SET_CLOCKTIME, timeString);
    Serial.println(cmd->toString());
    _commandHandler->send(cmd);
  }
}

void IOModule::refreshBatteries(){
  /* Refresh Batteries */
  bool batteriesArray[4] = {false, false, false, false};
  
  if(_batteries == 1){
    batteriesArray[random(0,2)] = true;
  } else if (_batteries == 2) {
    batteriesArray[0] = true;
    batteriesArray[1] = true;
  }
  /* Re-enable this when 4 batteries are available
  for(int i = 0; i < _batteries; i++){
    batteriesArray[i] = true;
  }

  if(_batteries != 0 || _batteries != 4) {
    for (int i=0; i < 4; i++) {
      int randomIndex = random(0, 4);
      bool temp = batteriesArray[randomIndex];
      batteriesArray[randomIndex] =  batteriesArray[i];
      batteriesArray[i] = temp;
    }
  }
  */

  _shiftHandler->setValue(0, 1, !batteriesArray[0]);
  _shiftHandler->setValue(0, 2, !batteriesArray[1]);
  _shiftHandler->setValue(0, 3, !batteriesArray[2]);
  _shiftHandler->setValue(0, 4, !batteriesArray[3]);
}

void IOModule::buildMenu(){
  _soundEnabledOption = new BoolOption(true);

  _batteriesOption = new IntOption("", _batteries, 0, 4, this); // Re-Enable when 4 batteries are available
  _batteriesOption = new IntOption((char * ) "", _batteries, 0, 2, this);
  _frkOption = new BoolOption(_frk, this);
  _carOption = new BoolOption(_car, this);
  _clrOption = new BoolOption(_clr, this);
  _serialPortOption = new BoolOption(_parallelPort, this);
  _parallelPortOption = new BoolOption(_serialPort, this);

  _outagesEnabledOption = new BoolOption(false);
  _alarmEnabledOption = new BoolOption(false);

  // _maxStrikesOption = new IntOption((char * ) "", 2, 0, 2, NULL);
  _durationMinutesOption = new IntOption((char * ) "Minutes:", 5, 0, 30, this);
  _durationSecondsOption = new IntOption((char * ) "Seconds:", 0, 0, 55, this, 5);

  _startBombOption = new ActionOption(this);

  
  Menu* menu = new Menu(_displayWriter);
  menu->setInputHandler(13, 9, A2, A3);

  menu->addCategory((new Category((char * ) "General"))
    ->addSetting(new Setting((char * ) "Sound", _soundEnabledOption))
    ->addSetting(new Setting((char * ) "Start", _startBombOption))
  );

  menu->addCategory((new Category((char * ) "Properties"))
    ->addSetting(new Setting((char * ) "Batteries", _batteriesOption))
    ->addSetting(new Setting((char * ) "FRK Label", _frkOption))
    ->addSetting(new Setting((char * ) "CAR Label", _carOption))
    ->addSetting(new Setting((char * ) "CLR Label", _clrOption))
    ->addSetting(new Setting((char * ) "Serial port", _serialPortOption))
    ->addSetting(new Setting((char * ) "Parallel port", _parallelPortOption))
  );

  menu->addCategory((new Category((char * ) "Difficutly"))
    ->addSetting(new Setting((char * ) "Duration", _durationMinutesOption))
    ->addSetting(new Setting((char * ) "Duration", _durationSecondsOption))
    ->addSetting(new Setting((char * ) "Max Strikes", _maxStrikesOption))
    ->addSetting(new Setting((char * ) "Power Outages", _outagesEnabledOption))
    ->addSetting(new Setting((char * ) "Alarm Clock", _alarmEnabledOption))
  );

  _menu = menu;
}